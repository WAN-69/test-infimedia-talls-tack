<?php

use App\Models\Toko;
use Illuminate\Http\Request;
use App\Http\Livewire\LaporanToko;
use App\Http\Livewire\InputLaporan;
use App\Http\Livewire\UserManagement;
use Illuminate\Support\Facades\Route;
use Illuminate\Contracts\Database\Query\Builder;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('newWelcome');
});

Route::prefix('get-data')->as('get.')->group(function () {
    Route::get('toko', fn(Request $request) => Toko::when($request->search,fn(Builder $query) => $query->where('nama_toko','like', "%{$request->search}%"))->orderBy('nama_toko')->get())->name('toko');
}); 

Route::middleware(['auth:sanctum',config('jetstream.auth_session'),'verified'])->group(function () {
    Route::get('/dashboard', LaporanToko::class)->name('dashboard')->middleware('role:super_admin|admin_toko|developer');
    Route::get('/input-laporan', InputLaporan::class)->name('input-laporan')->middleware('role:inputer|developer');
    Route::get('/user-management',UserManagement::class)->name('user-management')->middleware('role:super_admin|developer');
});
