const defaultTheme = require("tailwindcss/defaultTheme");

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php",
        "./vendor/laravel/jetstream/**/*.blade.php",
        "./storage/framework/views/*.php",
        "./resources/views/**/*.blade.php",
        "./resources/views/*.blade.php",
        "./vendor/wireui/wireui/resources/**/*.blade.php",
        "./vendor/wireui/wireui/ts/**/*.ts",
        "./vendor/wireui/wireui/src/View/**/*.php",
        './vendor/rappasoft/laravel-livewire-tables/resources/views/**/*.blade.php',
        // './app/Http/Livewire/**/*Table.php',
        // './vendor/power-components/livewire-powergrid/resources/views/**/*.php',
        // './vendor/power-components/livewire-powergrid/src/Themes/Tailwind.php'
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ["Nunito", ...defaultTheme.fontFamily.sans],
            },
        },
    },

    plugins: [
        require("@tailwindcss/forms"),
        require("@tailwindcss/typography"),
        // require("daisyui"),
    ],
    presets: [require("./vendor/wireui/wireui/tailwind.config.js")],

    // daisyui: {
    //     themes: [
    //         {
    //             mytheme: {
    //                 primary: "#bc1a1f",

    //                 secondary: "#309694",

    //                 accent: "#31b74e",

    //                 neutral: "#f3f4f6",

    //                 "base-100": "#FDFCFD",

    //                 info: "#ADC8E6",

    //                 success: "#25B195",

    //                 warning: "#F4C357",

    //                 error: "#F75F80",
    //             },
    //         },
    //     ],
    // },
};
