<?php

use App\Models\Toko;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_tokos', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Toko::class);
            $table->dateTime('date');
            $table->integer('total');
            $table->string('nota_file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_tokos');
    }
};
