<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Toko>
 */
class TokoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nama_toko' => 'Toko '. $this->faker->unique()->randomElement(['A','B','C']),
            'owner' => $this->faker->name(),
            'user_id' => $this->faker->unique()->randomElement([2,3,4]),
        ];
    }
}
