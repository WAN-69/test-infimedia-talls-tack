<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;
use Database\Seeders\RoleSeeder;
use Database\Seeders\TokoSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([RoleSeeder::class]);
        $this->call([TokoSeeder::class]);

        $user = User::factory()->create([
            'name' => 'Irwan',
            'username' => 'irwan_aja',
            'email' => 'test@example.com',
        ]);

        $adminTokoA = User::factory()->create([
            'name' => 'Admin Toko A',
            'username' => 'admin_toko_a',
            'email' => 'admin_toko_a@example.com',
        ]);
        $adminTokoB = User::factory()->create([
            'name' => 'Admin Toko B',
            'username' => 'admin_toko_b',
            'email' => 'admin_toko_b@example.com',
        ]);
        $adminTokoC = User::factory()->create([
            'name' => 'Admin Toko C',
            'username' => 'admin_toko_c',
            'email' => 'admin_toko_c@example.com',
        ]);
        $inputer = User::factory()->create([
            'name' => 'Inputer',
            'username' => 'saya_inputer',
            'email' => 'saya_inputer@example.com',
        ]);
        $superAdmin = User::factory()->create([
            'name' => 'Super Admin',
            'username' => 'super_admin',
            'email' => 'super_admin@example.com',
        ]);

        $user->assignRole('developer');
        $adminTokoA->assignRole('admin_toko');
        $adminTokoB->assignRole('admin_toko');
        $adminTokoC->assignRole('admin_toko');
        $inputer->assignRole('inputer');
        $superAdmin->assignRole('super_admin');
    }
}
