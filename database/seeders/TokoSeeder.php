<?php

namespace Database\Seeders;

use App\Models\Toko;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TokoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Toko::factory()->create([
            'nama_toko' => 'Toko A',
            'user_id' => 2,
        ]);
        Toko::factory()->create([
            'nama_toko' => 'Toko B',
            'user_id' => 3,
        ]);
        Toko::factory()->create([
            'nama_toko' => 'Toko C',
            'user_id' => 4,
        ]);
    }
}
