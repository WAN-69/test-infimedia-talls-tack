<?php

namespace App\Http\Controllers;

use App\Models\LaporanToko;
use App\Http\Requests\StoreLaporanTokoRequest;
use App\Http\Requests\UpdateLaporanTokoRequest;

class LaporanTokoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreLaporanTokoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLaporanTokoRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LaporanToko  $laporanToko
     * @return \Illuminate\Http\Response
     */
    public function show(LaporanToko $laporanToko)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LaporanToko  $laporanToko
     * @return \Illuminate\Http\Response
     */
    public function edit(LaporanToko $laporanToko)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateLaporanTokoRequest  $request
     * @param  \App\Models\LaporanToko  $laporanToko
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLaporanTokoRequest $request, LaporanToko $laporanToko)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LaporanToko  $laporanToko
     * @return \Illuminate\Http\Response
     */
    public function destroy(LaporanToko $laporanToko)
    {
        //
    }
}
