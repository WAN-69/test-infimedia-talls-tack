<?php

namespace App\Http\Livewire;

use App\Models\Toko;
use App\Models\User;
use Livewire\Component;
use WireUi\Traits\Actions;
use Spatie\Permission\Models\Role;

class UserManagement extends Component
{
    use Actions;

    public $cardModal = false;
    public $name, $username, $email, $role;
    public $nama_toko, $owner;
    public $showFormToko = false;

    public function updatedRole()
    {
        // $this->showFormToko = true;
    }

    public function save()
    {
        $this->validate([
            'name' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'required|unique:users,email',
            'role' => 'required',
        ]);

        $newUser = User::factory()->create([
            'name' => $this->name,
            'username' => $this->username,
            'email' => $this->email,
        ]);
        $newUser->assignRole($this->role);

        if ($this->role == 2) {
            Toko::create([
                'nama_toko' => $this->nama_toko,
                'owner' => $this->owner,
                'user_id' => $newUser->id,
            ]);
        }

        $this->resetForm();
        $this->notification()->success(
            $title = 'New User Has benn saved',
            $description = 'User was successfull added'
        );
    }

    public function resetForm()
    {
        $this->name = null;
        $this->username = null;
        $this->email = null;
        $this->nama_toko = null;
        $this->owner = null;
        $this->cardModal = false;
    }

    public function render()
    {
        return view('livewire.user-management', [
            'users' => User::with('roles')->get(),
            'roles' => Role::all(),
        ]);
    }
}
