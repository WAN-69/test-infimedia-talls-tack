<?php

namespace App\Http\Livewire;

use App\Models\Toko;
use Livewire\Component;
use Illuminate\Support\Carbon;
use App\Models\LaporanToko as Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class LaporanToko extends Component
{
    public $filter_date = 'Semua';
    public $filter_toko;

    public function download()
    {
        return response()->download(storage_path('exports/export.csv'));
    }

    public function render()
    {
        $data = Model::query()
        ->with('toko')
        ->when(!Auth::user()->hasRole(['super_admin','developer']),fn($query) => $query->whereHas('toko',fn(Builder $q) => $q->where('user_id',Auth::user()->id)))
        ->when($this->filter_date == 'Semua',fn($query) => $query)
        ->when($this->filter_date == 'Harian',fn($query) => $query->whereDay('date', '=',Carbon::now()))
        ->when($this->filter_date == 'Mingguan',fn($query) => $query->whereBetween('date',[Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()]))
        ->when($this->filter_date == 'Bulanan',fn($query) => $query->whereMonth('date','=',Carbon::now()->month))
        ->when($this->filter_toko, fn($query,$filter_toko) => $query->where('toko_id',$filter_toko))
        ->get();

        // dd($data);
        return view('livewire.laporan-toko',[
            'data' => $data,
            'stores' => Toko::all(),
        ]);
    }
}
