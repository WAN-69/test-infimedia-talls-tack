<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\LaporanToko;

class Chart extends Component
{
    public $label;
    public $total;

    public function mount()
    {
        $data = LaporanToko::with('toko')->get();
        $label = $data->pluck('toko.nama_toko')->unique();
        $total = $data->groupBy('toko_id')->map(fn($item) => $item->pluck('total')->sum());

        // dd($total);

        $this->label = json_encode($label);
        $this->total = json_encode($total);
    }

    public function render()
    {
        return view('livewire.chart');
    }
}
