<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\LaporanToko;
use Livewire\WithFileUploads;
use WireUi\Traits\Actions;

class InputLaporan extends Component
{
    use WithFileUploads,Actions;
    
    public $toko_id,$date,$total;
    public $nota;

    public function updatedNota()
    {
        $this->validate([
            'nota' => 'mimes:pdf',
        ]);
    }

    public function resetFile()
    {
        $this->nota = null;
    }

    public function resetForm()
    {
        $this->toko_id = null;
        $this->date = null;
        $this->total = null;
        $this->nota = null;
    }

    public function save()
    {
        $this->validate([
            'toko_id' => 'required',
            'date' => 'required',
            'total' => 'required',
        ]);

        $filename = 'Laporan_Toko_'.$this->toko_id.$this->date.'.'.$this->nota->extension();
        $this->nota->storeAs('nota',$filename,'local');

        LaporanToko::create([
            'toko_id' => $this->toko_id,
            'date' => $this->date,
            'total' => $this->total,
            'nota_file' => $filename,
        ]);

        $this->resetForm();
        $this->notification()->success(
            $title = 'Store Report saved',
            $description = 'Your Resport was successfull saved'
        );
    }

    public function render()
    {
        return view('livewire.input-laporan');
    }
}
