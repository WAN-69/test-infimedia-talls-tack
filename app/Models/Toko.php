<?php

namespace App\Models;

use App\Models\LaporanToko;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Toko extends Model
{
    use HasFactory;
    
    protected $fillable = ['nama_toko','owner','user_id'];

    /**
     * Get all of the storeReports for the Toko
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function storeReports(): HasMany
    {
        return $this->hasMany(LaporanToko::class);
    }
}
