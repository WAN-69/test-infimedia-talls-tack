<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @hasanyrole('super_admin|developer')
            <div class="bg-white overflow-hidden shadow-xl mb-5 sm:rounded-lg p-5 w-1/2 h-1/2">
                <livewire:chart/>
            </div>
            @endhasanyrole
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-5">
                <!-- component -->
                <div class="grid grid-cols-4 gap-3 px-5">
                    <x-select label="Filter Date" placeholder="Filter Data By Date" :options="['Semua','Harian', 'Mingguan', 'Bulanan']"
                    wire:model="filter_date" />
                    @hasanyrole('super_admin|developer')
                    <x-select label="Filter Toko" placeholder="Filter Data By Toko" :options="$stores"
                    wire:model="filter_toko" option-label="nama_toko" option-value="id" />
                    @endhasanyrole
                </div>
                <div class="overflow-x-auto">
                    <div class="flex items-center justify-centerfont-sans">
                        <div class="w-full p-5">
                            <div class="bg-white shadow-md rounded-lg">
                                <table class="min-w-max w-full table-auto">
                                    <thead>
                                        <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                                            <th class="py-3 px-6 text-left">Nama Toko</th>
                                            <th class="py-3 px-6 text-left">Date</th>
                                            <th class="py-3 px-6 text-center">Total Penjualan</th>
                                            <th class="py-3 px-6 text-center">Nota File</th>
                                            {{-- <th class="py-3 px-6 text-center">Actions</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody class="text-gray-600 text-sm font-light">
                                        @forelse ($data as $item)
                                            <tr class="border-b border-gray-200 hover:bg-gray-100">
                                                <td class="py-3 px-6 text-left whitespace-nowrap">
                                                    <div class="flex items-center">
                                                        <span class="font-medium">{{ $item->toko->nama_toko }}</span>
                                                    </div>
                                                </td>
                                                <td class="py-3 px-6 text-left">
                                                    <div class="flex items-center">
                                                        <span>{{ Carbon\Carbon::create($item->date)->format('D, d M Y') }}</span>
                                                    </div>
                                                </td>
                                                <td class="py-3 px-6 text-center">
                                                    <span> Rp. {{ number_format($item->total,0) }}</span>
                                                </td>
                                                <td class="py-3 px-6 text-center">
                                                    <button wire:click='download' class="bg-purple-200 text-purple-600 py-1 px-3 rounded-full text-xs cursor-pointer">{{ $item->nota_file }}</button>
                                                </td>
                                                {{-- <td class="py-3 px-6 text-center">
                                                    <div class="flex item-center justify-center">
                                                        <div
                                                            class="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
                                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                                                viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                                    stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                                    stroke-width="2"
                                                                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                                                            </svg>
                                                        </div>
                                                        <div
                                                            class="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
                                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                                                viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                                    stroke-width="2"
                                                                    d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                                            </svg>
                                                        </div>
                                                        <div
                                                            class="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
                                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                                                viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                                    stroke-width="2"
                                                                    d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </td> --}}
                                            </tr>
                                        @empty
                                            <span>Kosong</span>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
