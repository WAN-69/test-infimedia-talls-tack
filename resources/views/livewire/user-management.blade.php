<div>
    <x-modal.card title="Tambah User Baru" wire:model.defer="cardModal">
        <div class="grid grid-cols-1 sm:grid-cols-2 gap-4">
            <x-input wire:model='name' label="Name" placeholder="Your Fullname" />
            <x-input wire:model='username' label="Username" placeholder="Your Username" />

            <div class="col-span-1 sm:col-span-2">
                <x-input wire:model='email' label="Email" placeholder="example@mail.com" />
                <x-select label="Role" placeholder="Role User" :options="$roles" wire:model="role" option-label="name"
                    option-value="id" />
            </div>
            @if ($role == 2)
                <hr class="col-span-2">
                <x-input wire:model='nama_toko' label="Nama Toko" placeholder="Your Storename" />
                <x-input wire:model='owner' label="Owner" placeholder="Your Owner Name" />
            @endif
        </div>

        <x-slot name="footer">
            <div class="flex justify-between gap-x-4">
                <div class="flex">
                    <x-button flat label="Cancel" x-on:click="close" />
                    <x-button primary label="Save" wire:click="save()" />
                </div>
            </div>
        </x-slot>
    </x-modal.card>
    <div>
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('User Management') }}
            </h2>
        </x-slot>

        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-5">
                    <!-- component -->
                    <div class="px-5">
                        <x-button wire:click="$set('cardModal', true)" icon="pencil" primary label="User Baru" />
                    </div>
                    <div class="overflow-x-auto">
                        <div class="flex items-center justify-centerfont-sans">
                            <div class="w-full p-5">
                                <div class="bg-white shadow-md rounded-lg">
                                    <table class="min-w-max w-full table-auto">
                                        <thead>
                                            <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                                                <th class="py-3 px-6 text-left">Nama User</th>
                                                <th class="py-3 px-6 text-left">Username</th>
                                                <th class="py-3 px-6 text-center">Email</th>
                                                <th class="py-3 px-6 text-center">Role</th>
                                                {{-- <th class="py-3 px-6 text-center">Actions</th> --}}
                                            </tr>
                                        </thead>
                                        <tbody class="text-gray-600 text-sm font-light">
                                            @forelse ($users as $user)
                                                <tr class="border-b border-gray-200 hover:bg-gray-100">
                                                    <td class="py-3 px-6 text-left whitespace-nowrap">
                                                        <div class="flex items-center">
                                                            <div class="mr-2">
                                                                <img class="h-8 w-8 rounded-full object-cover" src="{{ $user->profile_photo_url }}" alt="{{ $user->name }}" />
                                                            </div>
                                                            <span class="font-medium">{{ $user->name }}</span>
                                                        </div>
                                                    </td>
                                                    <td class="py-3 px-6 text-left">
                                                        <div class="flex items-center">
                                                            {{-- <div class="mr-2">
                                                                <img class="w-6 h-6 rounded-full"
                                                                    src="https://randomuser.me/api/portraits/men/1.jpg" />
                                                            </div> --}}
                                                            <span>{{ $user->username }}</span>
                                                        </div>
                                                    </td>
                                                    <td class="py-3 px-6 text-center">
                                                        <span>{{ $user->email }}</span>
                                                        {{-- <div class="flex items-center justify-center">
                                                            <img class="w-6 h-6 rounded-full border-gray-200 border transform hover:scale-125"
                                                                src="https://randomuser.me/api/portraits/men/1.jpg" />
                                                            <img class="w-6 h-6 rounded-full border-gray-200 border -m-1 transform hover:scale-125"
                                                                src="https://randomuser.me/api/portraits/women/2.jpg" />
                                                            <img class="w-6 h-6 rounded-full border-gray-200 border -m-1 transform hover:scale-125"
                                                                src="https://randomuser.me/api/portraits/men/3.jpg" />
                                                        </div> --}}
                                                    </td>
                                                    <td class="py-3 px-6 text-center">
                                                        <span
                                                            class="bg-purple-200 text-purple-600 py-1 px-3 rounded-full text-xs">
                                                            {{ $user->roles[0]->name }}
                                                        </span>
                                                    </td>
                                                    {{-- <td class="py-3 px-6 text-center">
                                                        <div class="flex item-center justify-center">
                                                            <div
                                                                class="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
                                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                                                    viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                                        stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                                        stroke-width="2"
                                                                        d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                                                                </svg>
                                                            </div>
                                                            <div
                                                                class="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
                                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                                                    viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                                        stroke-width="2"
                                                                        d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                                                </svg>
                                                            </div>
                                                            <div
                                                                class="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
                                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                                                    viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                                        stroke-width="2"
                                                                        d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                                </svg>
                                                            </div>
                                                        </div>
                                                    </td> --}}
                                                </tr>
                                            @empty
                                                <span>Kosong</span>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
