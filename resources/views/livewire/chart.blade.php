<div>
    <canvas id="myChart"></canvas>
    @push('js')
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script>
            var labelChart = JSON.parse(`<?php echo $label; ?>`);
            var totalChart = JSON.parse(`<?php echo $total; ?>`);
            console.log(Object.keys(labelChart).map(key => labelChart[key]));
            console.log(Object.keys(totalChart).map(key => totalChart[key]));
            const ctx = document.getElementById('myChart');

            new Chart(ctx, {
                type: 'polarArea',
                data: {
                    labels: Object.keys(labelChart).map(key => labelChart[key]),
                    datasets: [{
                        label: 'Total Penjualan',
                        data: Object.keys(totalChart).map(key => totalChart[key]),
                        backgroundColor: [
                            'rgb(255, 99, 132)',
                            'rgb(75, 192, 192)',
                            'rgb(255, 205, 86)',
                            'rgb(201, 203, 207)',
                            'rgb(54, 162, 235)'
                        ]
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    },
                    plugins: {
                        title: {
                            display: true,
                            text: 'Total Penjualan Toko'
                        }
                    }
                }
            });
        </script>
    @endpush
</div>
