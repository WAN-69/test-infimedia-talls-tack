<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Input Laporan Toko') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-8">
                <div class="grid grid-cols-2 gap-4 ">
                    <div class="">
                        <x-select label="Select some Store" wire:model.defer="toko_id" placeholder="Search a Store"
                            :async-data="route('get.toko')" option-label="nama_toko" option-value="id" />
                    </div>
                    <div class="">
                        <x-datetime-picker label="Date" placeholder="Date" wire:model.defer="date" />
                    </div>
                    <div class="col-span-2">
                        <x-inputs.currency label="Total Penjualan" precision="0" icon="currency-dollar" thousands="."
                            decimal="," wire:model.defer="total" />
                    </div>
                    <div class="col-span-2">
                        <x-jet-label class="mb-1" for="nota" value="{{ __('Nota File') }}" />
                        @error('nota') <span class="text-red-500">{{ $message }}</span> @enderror
                        <div  x-data="{ files: null }"
                            class="grid grid-cols-2">
                            <label for="dropzone-file"
                                class="cursor-pointer flex w-full max-w-lg flex-col items-center rounded-xl border-2 border-dashed bg-white p-6 text-center {{$this->nota != null ? 'border-teal-400' : ''}} @error('nota') border-rose-400 @enderror ">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-10 w-10 text-blue-500" fill="none"
                                    viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                        d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12" />
                                </svg>

                                <h2 class="mt-4 text-xl font-medium text-gray-700 tracking-wide">Nota File</h2>

                                <p class="mt-2 text-gray-500 tracking-wide">Upload Nota File (.pdf)</p>

                                <input id="dropzone-file"
                                    type="file" x-on:change="files = Object.values($event.target.files)" wire:model='nota' class="hidden" />
                            </label>
                            <img wire:loading wire:target="nota" class="mx-auto my-auto" src="/Fidget-spinner.gif" alt="loading">
                            <div x-show='$wire.nota != null' class=" h-12 rounded-md bg-[#F5F7FB] py-3 px-4">
                                <div class="flex items-center justify-between">
                                    <span x-text="files?.map(file => file.name).join(', ')"
                                        class="truncate pr-3 text-base font-medium text-[#07074D]"></span>
                                    <button x-on:click="files = null" wire:click="resetFile" type="reset"
                                        class="text-[#07074D] hover:text-red-400">
                                        <svg width="10" height="10" viewBox="0 0 10 10" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M0.279337 0.279338C0.651787 -0.0931121 1.25565 -0.0931121 1.6281 0.279338L9.72066 8.3719C10.0931 8.74435 10.0931 9.34821 9.72066 9.72066C9.34821 10.0931 8.74435 10.0931 8.3719 9.72066L0.279337 1.6281C-0.0931125 1.25565 -0.0931125 0.651788 0.279337 0.279338Z"
                                                fill="currentColor" />
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M0.279337 9.72066C-0.0931125 9.34821 -0.0931125 8.74435 0.279337 8.3719L8.3719 0.279338C8.74435 -0.0931127 9.34821 -0.0931123 9.72066 0.279338C10.0931 0.651787 10.0931 1.25565 9.72066 1.6281L1.6281 9.72066C1.25565 10.0931 0.651787 10.0931 0.279337 9.72066Z"
                                                fill="currentColor" />
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex items-center justify-end mt-4">
                    <x-button wire:click='save'  label="Save" positive right-icon="check-circle" />
                </div>
            </div>
        </div>
    </div>
</div>
