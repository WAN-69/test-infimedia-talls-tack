<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test Infimedia</title>
    <wireui:scripts />
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body class="antialiased">
    <div class="relative bg-gray-100">
        <section class="relative bg-sky-50">
            <div class="relative pt-16 pb-32 flex content-center items-center justify-center min-h-screen-75">
                <div class="absolute top-0 w-full h-full bg-center bg-cover"
                    style="
                background-image: url('https://images.unsplash.com/photo-1557804506-669a67965ba0?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1267&amp;q=80');">
                    <span id="blackOverlay" class="w-full h-full absolute opacity-75 bg-black"></span>
                </div>
                <div class="container relative mx-auto">
                    <div class="items-center flex flex-wrap">
                        <div class="w-full lg:w-6/12 px-4 ml-auto mr-auto text-center">
                            <div class="pr-12">
                                <h1 class="text-white font-semibold text-5xl">
                                    Tingkatkan Bisnis Tokomu Sekarang !
                                </h1>
                                <p class="mt-4 text-lg text-blue-200">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt animi, cupiditate maiores veritatis voluptatem mollitia, veniam temporibus recusandae hic, fuga tempora quidem dolor aperiam ab.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-70-px"
                    style="transform: translateZ(0px)">
                    <svg class="absolute bottom-0 overflow-hidden" xmlns="http://www.w3.org/2000/svg"
                        preserveAspectRatio="none" version="1.1" viewBox="0 0 2560 100" x="0" y="0">
                        <polygon class="text-blueGray-200 fill-current" points="2560 0 2560 100 0 100"></polygon>
                    </svg>
                </div>
            </div>
            <section class="pb-10 bg-blueGray-200 -mt-24">
                <div class="container mx-auto px-4">
                    <div class="flex flex-wrap">
                        <div class="lg:pt-12 pt-6 w-full md:w-4/12 px-4 text-center">
                            <div
                                class="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                                <div class="px-4 py-5 flex-auto">
                                    <div
                                        class="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                                        <x-icon name="home" class="w-5 h-5" solid />
                                    </div>
                                    <h6 class="text-xl font-semibold">Bisa Dikendalikan Dimana Saja</h6>
                                    <p class="mt-2 mb-4 text-blueGray-500">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur, iusto.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="w-full md:w-4/12 px-4 text-center">
                            <div
                                class="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                                <div class="px-4 py-5 flex-auto">
                                    <div
                                        class="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-sky-400">
                                        <x-icon name="presentation-chart-bar" class="w-5 h-5" solid />
                                    </div>
                                    <h6 class="text-xl font-semibold">Mudah Dipakai</h6>
                                    <p class="mt-2 mb-4 text-blueGray-500">
                                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. In, quo!
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="pt-6 w-full md:w-4/12 px-4 text-center">
                            <div
                                class="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                                <div class="px-4 py-5 flex-auto">
                                    <div
                                        class="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-emerald-400">
                                        <x-icon name="badge-check" class="w-5 h-5" solid />
                                    </div>
                                    <h6 class="text-xl font-semibold">Sudah Terpercaya</h6>
                                    <p class="mt-2 mb-4 text-blueGray-500">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita, aut!
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <div class="fixed top-0 right-0 px-6 py-4 sm:block">
            @auth
                <a href="{{ auth()->user()->hasRole('inputer') ? url('/input-laporan') : url('/dashboard') }}" class="text-sm rounded-lg bg-sky-300 font-semibold dark:text-gray-500 px-3 py-2 text-blue-800">{{ auth()->user()->hasRole('inputer') ? 'Input Laporan' : 'Dashboard' }}</a>
            @else
                <a href="{{ route('login') }}" class="text-sm rounded-lg bg-sky-300 font-semibold dark:text-gray-500 px-3 py-2 text-blue-800">Log in</a>
    
                {{-- @if (Route::has('register'))
                    <a href="{{ route('register') }}"
                        class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                @endif --}}
            @endauth
        </div>
    </div>
</body>

</html>
