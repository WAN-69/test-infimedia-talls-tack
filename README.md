
# Repository Test PT Infimedia

Repository ini ditujukan hanya sebagai syarat Test di PT Infimedia


## Setup 

Clone project ini

```bash
  git clone https://gitlab.com/WAN-69/test-infimedia-talls-tack.git
```

Masuk ke project direktori

```bash
  cd test-infimedia-talls-tack
```

Install dependencies

```bash
  npm install || Yarn Install
  composer install
```

Salin file .env-example menjadi .env

```bash
  cp .env-example .env
```

Generate Laravel Key

```bash
  php artisan key:generate
```

Jalankan migration laravel && Seeder Laravel

```bash
  php artisan migrate --seed
```

Start the server

```bash
  npm run start
  php artisan serve
```

